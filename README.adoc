= cmake-gradle-plugin

A plugin to wrap up CMake functionality inside Gradle is a very gradlesque way. Will also deal with downloading CMake on supported platforms.

== Objectives

The objectives of the cmake gradle plugin includes

* Build cmake based projects from Gradle
* Build cmake projects without manually installing cmake
* Build with various versions of cmake (possibly to check changes in build behaviour)
* Ability to run CTest and CPack too from gradle.

